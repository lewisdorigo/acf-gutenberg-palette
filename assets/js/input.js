(function($){


	/**
	*  initialize_field
	*
	*  This function will initialize the $field.
	*
	*  @date	30/11/17
	*  @since	5.6.5
	*
	*  @param	n/a
	*  @return	n/a
	*/

	function initialize_field( $field ) {

		$field.on('change', 'input[type="radio"]', function() {
			var $this = $(this),
				$field = $this.closest('.drgo-color-picker'),
				$item = $this.closest('.drgo-color-picker__item'),
				$items = $('.drgo-color-picker__item', $field);

			$items.removeClass('drgo-color-picker__item--checked');
			$item.addClass('drgo-color-picker__item--checked');
		});

		$field.on('click', '.drgo-color-picker__clear', function(e) {
			e.preventDefault();

			var $this = $(this),
				$field = $this.closest('.drgo-color-picker'),
				$inputs = $('input[type="radio"]', $field),
				$items = $('.drgo-color-picker__item', $field);

			$inputs.prop('checked', false);
			$items.removeClass('drgo-color-picker__item--checked');

			$('.acf-gutenberg-palette-none', $field).prop('checked', true);
		});

	}


	if( typeof acf.add_action !== 'undefined' ) {

		/*
		*  ready & append (ACF5)
		*
		*  These two events are called when a field element is ready for initizliation.
		*  - ready: on page load similar to $(document).ready()
		*  - append: on new DOM elements appended via repeater field or other AJAX calls
		*
		*  @param	n/a
		*  @return	n/a
		*/

		acf.add_action('ready_field/type=gutenberg_palette', initialize_field);
		acf.add_action('append_field/type=gutenberg_palette', initialize_field);


	} else {

		/*
		*  acf/setup_fields (ACF4)
		*
		*  These single event is called when a field element is ready for initizliation.
		*
		*  @param	event		an event object. This can be ignored
		*  @param	element		An element which contains the new HTML
		*  @return	n/a
		*/

		$(document).on('acf/setup_fields', function(e, postbox){

			// find all relevant fields
			$(postbox).find('.field[data-field_type="gutenberg_palette"]').each(function(){

				// initialize
				initialize_field( $(this) );

			});

		});

	}

})(jQuery);
