<?php namespace Dorigo\ACF\GutenbergPaletteField;

/*
Plugin Name: Advanced Custom Fields: Gutenberg Palette
Plugin URI: https://dorigo.co/
Description: ACF Field type to select a color from the theme’s Gutenberg editor palette.
Version: 1.1.0
Author: Lewis Dorigo
Author URI: https://dorigo.co
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/

// exit if accessed directly
if( ! defined( 'ABSPATH' ) ) exit;


// check if class already exists
if( !class_exists('NAMESPACE_acf_plugin_FIELD_NAME') ) :

class GutenbergPaletteField {

	// vars
	var $settings;


	/*
	*  __construct
	*
	*  This function will setup the class functionality
	*
	*  @type	function
	*  @date	17/02/2016
	*  @since	1.0.0
	*
	*  @param	void
	*  @return	void
	*/

	function __construct() {

		// settings
		// - these will be passed into the field class.
		$this->settings = array(
			'version'	=> '1.0.0',
			'url'		=> plugin_dir_url( __FILE__ ),
			'path'		=> plugin_dir_path( __FILE__ )
		);

		// include field
		add_action('acf/include_field_types', [$this, 'include_field']); // v5
	}


	/*
	*  include_field
	*
	*  This function will include the field type class
	*
	*  @type	function
	*  @date	17/02/2016
	*  @since	1.0.0
	*
	*  @param	$version (int) major ACF version. Defaults to false
	*  @return	void
	*/

	function include_field( $version = false ) {

		// support empty $version
		if(!$version || $version != 5) { return false; }

		// include
		include_once('fields/acf-field-gutenberg-palette-v5.php');
	}

}


// initialize
new GutenbergPaletteField();


// class_exists check
endif;

?>